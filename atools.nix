{ lib
, stdenv
, fetchFromGitLab
, lua5_3
, redo-sh
, bats
, scdoc
, makeWrapper
, busybox
, runtimeShell
, go_1_18
, doCheck ? (stdenv.hostPlatform == stdenv.buildPlatform)
}:

stdenv.mkDerivation rec {
  pname = "atools";
  version = "20.2.0";

  src = fetchFromGitLab {
    domain = "gitlab.alpinelinux.org";
    owner = "Leo";
    repo = pname;
    rev = version;
    sha256 = "0fipcwffci1fcaxwbqd3l6mp99avs3x8k5xzkciwd4lbs8glshv0";
  };

  nativeBuildInputs =
    [
      lua5_3
      redo-sh
      bats
      scdoc
      makeWrapper
      go_1_18
    ] ++ lib.optional stdenv.isLinux [ busybox ];

  patchPhase = ''
    # replace `/bin/ash` in the shebang with the harcoded path
    # to the store
    for prog in \
      aports-lint \
      apkbuild-lint \
      initd-lint \
      apkbuild-fixer \
      convert-volatile-source; 
    do
    ${if stdenv.isLinux then 
      "substituteInPlace $prog --replace '/usr/bin/env ash' ${busybox}/bin/ash"
      else
      "substituteInPlace $prog --replace '/usr/bin/env ash' ${runtimeShell}"
    }
    done

    # Same as the above but with `/usr/bin/lua5.3`
    substituteInPlace secfixes-check --replace '/usr/bin/lua5.3' ${lua5_3}/bin/lua
  '';

  buildPhase = ''
    export GOCACHE=$(mktemp -d)
    redo build
  '';

  checkPhase = ''
    redo check
  '';

  installPhase = ''
    PREFIX=/ DESTDIR="$out" redo install

    ${lib.optionalString stdenv.isLinux
    ''
    # Wrap all binaries in linux to include binaries provided by
    # busybox in its prefix, this is to guarantee the binaries
    # have a base level of tooling that matches what is used
    # in Alpine Linux
    for prog in "$out"/bin/*; do
      # do not wrap the lua script
      [ "$prog" = "$out"/bin/secfixes-check ] && continue
      # do not wrap the go binary
      [ "$prog" = "$out"/bin/print-go-deps ] && continue
      wrapProgram "$prog" --prefix PATH : ${lib.makeBinPath [ busybox ]}
    done''};
  '';

  meta = {
    homepage = "https://gitlab.alpinelinux.org/Leo/atools";
    description = "Auxiliary scripts for abuild";
    license = lib.licenses.mit;
    platforms = lib.platforms.unix;
  };
}
