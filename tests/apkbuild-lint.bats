#!/usr/bin/env bats

cmd=./apkbuild-lint
apkbuild=$BATS_TMPDIR/APKBUILD

# Remove APKBUILD_STYLE variable from the environment as that can affect results
unset APKBUILD_STYLE

assert_match() {
	output=$1
	expected=$2

	echo "$output" | grep -qE "$expected"
}

is_travis() {
	test -n "$TRAVIS"
}

retab() {
	case $(realpath "$(which unexpand)") in
		*busybox*) flag="-f";;
		*) flag="--first-only";;
	esac

	unexpand $flag -t4
}

@test 'default builddir can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1
	builddir=/$pkgname-$pkgver

	build() {
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL1\].*:builddir can be removed as it is the default value"
}

@test 'default _builddir can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1
	_builddir=/$pkgname-$pkgver

	build() {
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL1\].*:builddir can be removed as it is the default value"
}

@test 'cd \"\$builddir\" is not highlighted' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	subpackages="py-${pkgname}:_py"

	_py() {
		cd "$builddir" # required
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'cd \"\$builddir\" after cd should be ignored' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd "$builddir/bar"
		foo
		cd "$builddir"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'cd \"\$builddir\" with brackets should be detected' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd "${builddir}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir\" can be removed in phase"
}

@test 'cd \"\$builddir\" with underline should be detected' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd "$_builddir"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir\" can be removed in phase"
}

@test 'cd \"\$builddir\" with brackets and no quotes should be detected' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd ${builddir}
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir\" can be removed in phase"
}

@test 'cd \"\$builddir\" without quotes should be detected' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd $builddir
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir\" can be removed in phase"
}

@test 'cd \"\$builddir\" should be highlighted if it is also the first' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd $builddir
		cd ${builddir}
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir\" can be removed in phase"
	assert_match "${lines[1]}" "\[AL13\].*builddir\" can be removed in phase"
}

@test 'unnecessary || return 1 can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		foo || return 1
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL2\].*|| return 1 is not required as set -e is used"
}

@test 'unnecessary || exit 1 can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		foo || exit 1
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL2\].*|| exit 1 is not required as set -e is used"
}

@test 'plain pkgname should not be quoted' {
	cat <<-"EOF" >$apkbuild
	pkgname="a"
	pkgver=1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL3\].*:pkgname must not be quoted"
}

@test 'quoted composed pkgname is fine' {
	skip "false positive"
	cat <<-"EOF" >$apkbuild
	pkgname="a"
	_flavor=foo
	pkgname="$pkgname-$_flavor"
	pkgver=1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgver should not be quoted' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver="aa"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL4\].*:pkgver must not be quoted"
}

@test 'empty global variable can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1
	install=
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL5\].*:variable set to empty string: "
}

@test 'custom global variables should start with an underscore' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1
	depends_foo=example
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL6\].*:prefix custom variable with _: "
}

@test 'indentation should be with tabs' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
        foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL7\].*:indent with tabs"
}

@test 'trailing whitespace should be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		foo 
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL8\].*:trailing whitespace"
}

@test 'prefer \$() to backticks' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		local a=`echo test`
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL25\].*:use.*instead of backticks"
}

@test 'backticks in comments should be ignored' {
	skip 'false positive'
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		# `foo` needs to be executed before bar
		foo
		bar
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'function keyword should not be used' {
	is_travis && skip "Broken on CI"
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	function build() {
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL9\].*:do not use the function keyword"
}

@test 'no space between function name and parenthesis' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build () {
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL10\].*:do not use space before function parenthesis"
}

@test 'one space after function parenthesis' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build()  {
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL11\].*:use one space after function parenthesis"
}

@test 'opening brace for function should be on the same line' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build()
	{
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL12\].*:do not use a newline before function opening brace"
}

@test 'cd to builddir dir without cd to other dir can be removed' {
	cat <<-"EOF" >$apkbuild
	pkgname=a
	pkgver=1

	build() {
		cd "$builddir"
		foo
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL13\].*builddir.*can be removed in phase"
}

@test 'pkgname must not have uppercase characters' {
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

	cat <<-"EOF" >$apkbuild
	pkgname=Foo
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"

	cat <<-"EOF" >$apkbuild
	pkgname=foo-FONT
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"

	cat <<-"EOF" >$apkbuild
	pkgname=f_oO
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"
	cat <<-"EOF" >$apkbuild
	pkgname=f.o.O
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"

	cat <<-"EOF" >$apkbuild
	pkgname=9Foo
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"

	cat <<-"EOF" >$apkbuild
	pkgname=FoO
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL14\].*:pkgname must not have uppercase characters"
}

@test 'pkgname uppercase exceptions (builtin exception)' {
	cat <<-"EOF" >$apkbuild
	pkgname=R
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgname uppercase exceptions (custom exceptions)' {
	cat <<-"EOF" >$apkbuild
	pkgname=ThisIsUppercase
	EOF

	CUSTOM_UPPERCASE_PKGNAMES=ThisIsUppercase run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgver must not have -rN' {
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
	
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=1-r3
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL15\].*:pkgver must not have -r or _r"

	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=0.1_r3a1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL15\].*:pkgver must not have -r or _r"

	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=02-r3a1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL15\].*:pkgver must not have -r or _r"
}

@test 'pkgver can have _rc but not -rc' {
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=1_rc1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

	cat <<-"EOF" >$apkbuild
	pkgname=foo
	pkgver=02-rc1
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL15\].*:pkgver must not have -r or _r"
}

@test '_builddir is set' {
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	_realname=Foo
	pkgver=1.0.0

	_builddir="$srcdir/$_realname-$pkgver"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL26\].*:rename _builddir to builddir"
}

@test '_builddir and builddir are set' {
	cat <<-"EOF" >$apkbuild
	pkgname=foo
	_realname=Foo
	pkgver=1.0.0

	builddir="$srcdir/$_realname-$pkgver"
	_builddir="$builddir/build"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgver is literal integer but is double-quoted' {
	cat <<-"EOF" >$apkbuild
	pkgver="1"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL28\].*:literal integers must not be quoted"
}

@test 'pkgver is literal integer but is single-quoted' {
	cat <<-"EOF" >$apkbuild
	pkgver='1'
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL28\].*:literal integers must not be quoted"
}

@test 'pkgver is literal integer and is not quoted' {
	cat <<-"EOF" >$apkbuild
	pkgver=2
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgver is not an literal integer and is not quoted' {
	cat <<-"EOF" >$apkbuild
	pkgver=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test '\"\$pkgname\" can be used as archive name in source' {
	cat <<-"EOF" >$apkbuild
	source="$pkgname-$pkgver.tar.gz::http://domain.com/my_package/$pkgver.tar.gz"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test '\"\$pkgname\" should not be used in source url' {
	cat <<-"EOF" >$apkbuild
	source="http://domain.com/$pkgname/$pkgver"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL29\].*:.*$pkgname should not be used in the source url"
}

@test '\"\$pkgname\" should not be used in source url (multiline)' {
	cat <<-"EOF" >$apkbuild
	source="
		http://domain.com/$pkgname/$pkgver
		"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL29\].*:.*$pkgname should not be used in the source url"
}

@test '\"\$pkgname\" should not be used in source url with archive name' {
	cat <<-"EOF" >$apkbuild
	source="$pkgname-$pkgver.tar.gz::http://domain.com/$pkgname/$pkgver"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL29\].*:.*$pkgname should not be used in the source url"
}

@test '\"\$pkgname\" should not be used in source url with archive name (multiline)' {
	cat <<-"EOF" >$apkbuild
	source="
		$pkgname-$pkgver.tar.gz::http://domain.com/$pkgname/$pkgver
		"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL29\].*:.*$pkgname should not be used in the source url"
}

@test 'double underscore in variable' {
	cat <<-"EOF" >$apkbuild
	__invalid=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"
}

@test 'double underscore in tabbed variable' {
	cat <<-"EOF" >$apkbuild
		__invalid=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"
}

@test 'no underscore in variable' {
	cat <<-"EOF" >$apkbuild
	pkgver=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'no underscore in tabbed variable' {
	cat <<-"EOF" >$apkbuild
		pkgver=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'double underscore in local variable' {
	cat <<-"EOF" >$apkbuild
	foo() {
	local __invalid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"

}

@test 'double underscore in local tabbed variable' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local __invalid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"
}

@test 'double underscore in local tabbed variable with valid variable after' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local __invalid valid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"
}

@test 'double underscore in local tabbed variable with valid variable before' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local valid __invalid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL30\].*:.*double underscore on variables are reserved"
}

@test 'double underscore as argument should be allowed' {
	cat <<-"EOF" >$apkbuild
	foo() {
		make -D__VAR=val
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'capitalized variable' {
	cat <<-"EOF" >$apkbuild
	Foo=bar
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	# Match lines[1] instead of lines[0] because check for variables that should have
	# underscore comes before it
	assert_match "${lines[1]}" "\[AL31\].*:.*variables must not have capital letters"
}

@test 'underscored capitalized variable' {
	cat <<-"EOF" >$apkbuild
	_Foo=bar
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL31\].*:.*variables must not have capital letters"
}


@test 'normal variable' {
	cat <<-"EOF" >$apkbuild
	pkgver=1.0
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'normal underscored variable' {
	cat <<-"EOF" >$apkbuild
	_commit=aabbcc
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'capitalized local variable' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local NotValid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL31\].*:.*variables must not have capital letters"
}

@test 'capitalized local variable between valid ones' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local valid NotValid correct
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL31\].*:.*variables must not have capital letters"
}

@test 'valid local variables' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local valid
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid variable bracing with underscore' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}_alpine"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid variable bracing with letter' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}Alpine"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid variable bracing with number' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}0alpine"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid variable bracing with capital letter' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}Alpine"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'invalid variable bracing due to hyphen' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}-Alpine"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
}

@test 'invalid variable bracing due to end of line' {
	cat <<-"EOF" >$apkbuild
	pkgname=${pypi_name}
	foo() {
		local pkgname=${pypi_name}
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
	assert_match "${lines[1]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
}

@test 'invalid variable bracing due to double-quotes' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name}"
	foo() {
		local pkgname="${pypi_name}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
	assert_match "${lines[1]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
}

@test 'valid variable bracing with local keyword and underscore' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local pkgname="${pypi_name}_Alpine"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid variable bracing with local keyword and letter' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local _py3ver pkgname="${pypi_name}Alpine"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'invalid variable bracing within other variables in a local statement' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local _py3ver pkgname="${pypi_name}" _test
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL32\].*:.*unnecessary usage of braces: ${pypi_name}"
}

@test 'valid variable bracing via parameter substitution' {
	cat <<-"EOF" >$apkbuild
	pkgname="${pypi_name#py3-}"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid local variable bracing via parameter substitution' {
	cat <<-"EOF" >$apkbuild
	foo() {
		local _py3ver pkgname="${pypi_name%%-1}" _test
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'CFLAGS can be expanded' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CFLAGS="$CFLAGS -O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

}

@test 'CFLAGS should not be overwritten' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	assert_match "${lines[0]}" '\[AL36\].*:CFLAGS should not be overwritten, add \$CFLAGS to it'
}

@test 'CFLAGS can be substituted (or removed)' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CFLAGS="${CFLAGS/-Os/-O3}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'GOFLAGS can be expanded' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    GOFLAGS="$GOFLAGS -O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

}

@test 'GOFLAGS should not be overwritten' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    GOFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" '\[AL36\].*:GOFLAGS should not be overwritten, add \$GOFLAGS to it'
}

@test 'GOFLAGS can be substituted (or removed)' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    GOFLAGS="${GOFLAGS/-buildmode=pie}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'CPPFLAGS can be expanded' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CPPFLAGS="$CPPFLAGS -O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

}

@test 'CPPFLAGS should not be overwritten' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CPPFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" '\[AL36\].*:CPPFLAGS should not be overwritten, add \$CPPFLAGS to it'
}

@test 'CPPFLAGS can be substituted (or removed)' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CPPFLAGS="${CPPFLAGS/-Os/-O3}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'CXXFLAGS can be expanded' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CXXFLAGS="$CXXFLAGS -O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

}

@test 'CXXFLAGS should not be overwritten' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CXXFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" '\[AL36\].*:CXXFLAGS should not be overwritten, add \$CXXFLAGS to it'
}

@test 'CXXFLAGS can be substituted (or removed)' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    CXXFLAGS="${CXXFLAGS/-Os/-O3}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'FFLAGS can be expanded' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    FFLAGS="$FFLAGS -O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]

}

@test 'FFLAGS should not be overwritten' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    FFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" '\[AL36\].*:FFLAGS should not be overwritten, add \$FFLAGS to it'
}

@test 'FFLAGS can be substituted (or removed)' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    FFLAGS="${FFLAGS/-Os/-O3}"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'Do not match partial names for flags' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    LUA_CFLAGS="-O2"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'FFLAGS should not be overwritten, second variable' {
	cat <<-"EOF" | retab >$apkbuild
	foo() {
	    IGNORE_FFLAGS=1 FFLAGS="-O3"
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" '\[AL36\].*:FFLAGS should not be overwritten, add \$FFLAGS to it'
}

@test 'no invalid options' {
	cat <<-"EOF" | retab >$apkbuild
	options="net checkroot !tracedeps"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'default_prepare is there' {
	cat <<-"EOF" | retab >$apkbuild
	prepare() {
	    default_prepare
	    autoreconf -fi
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'default_prepare is there, but it has a prefixing variable' {
	cat <<-"EOF" | retab >$apkbuild
	prepare() {
	    DEFAULT_PREPARE_NEVER_HAS_VARIABLES=1 default_prepare
	    autoreconf -fi
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL54\].*:prepare\(\) is missing call to 'default_prepare'"
}

@test 'default_prepare is missing' {
	cat <<-"EOF" | retab >$apkbuild
	prepare() {
	    do_stuff
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL54\].*:prepare\(\) is missing call to 'default_prepare'"
}

@test 'default_prepare is there but is not a command' {
	cat <<-"EOF" | retab >$apkbuild
	prepare() {
	    echo default_prepare
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL54\].*:prepare\(\) is missing call to 'default_prepare'"
}

@test 'default_prepare is there but is commented' {
	cat <<-"EOF" | retab >$apkbuild
	prepare() {
	    # default_prepare
	    :
	}
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL54\].*:prepare\(\) is missing call to 'default_prepare'"
}

@test 'one invalid option' {
	cat <<-"EOF" | retab >$apkbuild
	options="invalid"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL49\].*:1:invalid option 'invalid'"
}

@test 'invalid option in a second assignment' {
	cat <<-"EOF" | retab >$apkbuild
	options="net"
	options="$options invalid"
	EOF

	run $cmd $apkbuild
	[[ $status -eq 1 ]]

	echo "output: $output"
	assert_match "${lines[0]}" "\[AL49\].*:2:invalid option 'invalid'"
}

@test 'custom valid options' {
	cat <<-"EOF" | retab >$apkbuild
	options="custom_two net custom_one !check"
	EOF

	export CUSTOM_VALID_OPTIONS="custom_one custom_two"
	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'all arches are valid' {
	cat <<-__EOF__ > $apkbuild
		arch="all"
		arch="all !x86_64"
		arch="x86 x86_64"
		arch="armhf"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'invalid arch' {
	cat <<-__EOF__ > $apkbuild
		arch="alli"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL57\].*:1:invalid arch 'alli'"
}

@test 'invalid arch (start)' {
	cat <<-__EOF__ > $apkbuild
		arch="alli !x86_64"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL57\].*:1:invalid arch 'alli'"
}

@test 'invalid arch (middle)' {
	cat <<-__EOF__ > $apkbuild
		arch="all !x86_65 !x86"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL57\].*:1:invalid arch '!x86_65'"
}

@test 'invalid arch (end)' {
	cat <<-__EOF__ > $apkbuild
		arch="all !x87"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL57\].*:1:invalid arch '!x87'"
}

@test 'No patch from volatile source' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'Patch from volatile source (GitHub)' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz
			https://github.com/user/repo/pull/200.patch
			"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL62\].*:volatile source 'https://github.com/user/repo/pull/200.patch'"
}

@test 'Patch from volatile source (GitLab)' {
	cat <<-__EOF__ > $apkbuild
		source="https://gitlab.com/user/repo/-/archive/v$pkgver/$pkgver.tar.gz
			https://gitlab.com/user/repo/-/merge_requests/200.patch
			https://gitlab.com/user/repo/-/merge_requests/100.diff
			"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL62\].*:volatile source 'https://gitlab.com/user/repo/-/merge_requests/200.patch'"
	assert_match "${lines[1]}" "\[AL62\].*:volatile source 'https://gitlab.com/user/repo/-/merge_requests/100.diff'"
}

@test 'Patch from volatile source (Alpine Linux GitLab)' {
	cat <<-__EOF__ > $apkbuild
		source="https://gitlab.alpinelinux.org/user/repo/-/archive/v$pkgver/$pkgver.tar.gz
			https://gitlab.alpinelinux.org/user/repo/-/merge_requests/200.patch
			https://gitlab.alpinelinux.org/user/repo/-/merge_requests/200.diff
			"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL62\].*:volatile source 'https://gitlab.alpinelinux.org/user/repo/-/merge_requests/200.patch'"
	assert_match "${lines[1]}" "\[AL62\].*:volatile source 'https://gitlab.alpinelinux.org/user/repo/-/merge_requests/200.diff'"
}

@test 'Volatile source uses ::, should not be treated as local patch' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz
			fix-build.patch::https://github.com/user/repo/commits/SHA1.patch
			"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'Volatile source that has .patch or .diff in the name but are not plaintext patches' {
	cat <<-__EOF__ > $apkbuild
		source="https://github.com/user/repo/archive/$pkgver.tar.gz
			https://github.com/user/repo/commits/SHA1.patch.gz
			https://gitlab.alpinelinux.com/user/repo/commits/SHA256.patch-foo
			"
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'pkgver is in a bad version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=doesnotexist
		pkgver=1.0.0
	__EOF__

	CUSTOM_BAD_VERSIONS="doesnotexist@1.0.0" run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '1.0.0' matches a bad version regex"
}

@test 'pkgver is in a bad version, hidden by a sourced variable' {
	cat <<-__EOF__ > $apkbuild
		pkgname=doesnotexist
		_pkgver=0
		pkgver=1.0.\$_pkgver
	__EOF__

	CUSTOM_BAD_VERSIONS="doesnotexist@1.0.0" run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '1.0.0' matches a bad version regex"
}

@test 'pkgver is in a bad version, hidden by a shell substitution' {
	cat <<-__EOF__ > $apkbuild
		pkgname=doesnotexist
		_pkgver=1-0-0
		pkgver=${_pkgver//-/.}
	__EOF__

	CUSTOM_BAD_VERSIONS="doesnotexist@1.0.0" run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '1.0.0' matches a bad version regex"
}

@test 'pkgver is not in a bad version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=doesnotexist
		pkgver=1.0.0
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

# vim: noexpandtab
