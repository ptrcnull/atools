initd-lint(1)

# NAME

initd-lint - A linter for openrc init files shipped with Alpine Linux

# SYNOPSIS

*initd-lint* <initd...>

# DESCRIPTION

initd-lint is a linter for openrc init files shipped with Alpine Linux See
*alint(5)* under the initd-lint section for an explanation of each violation
that initd-lint checks for.

initd-lint will print the policy violations found and exit 1, if no violations
are found then nothing will be printed and it will exit 0.

# OUTPUT

initd-lint will print to stdout whenever a policy violation is found in the
following format

	SEVERITYCERTAINITY:[TAG]:PATH::MSG

- *SEVERITY* refers to how severe a violation is, ranging from *S* to *M*.
- *CERTAINITY* refers to how likely it is not a false positive, ranging from *C* to *P*
- *TAG* refers to the tag of the violation, see *alint(5)* for more details.
- *PATH* refers to the path given for *apkbuild-lint* to check.
- *MSG* is a short message meant for humans to know what is the violation.

# AUTHORS

Maintained by Kevin Daudt <kdaudt@alpinelinux.org>

# SEE ALSO

*alint(5)*
