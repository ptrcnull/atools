{
  description = "Collection of linters and fixers for dealing with APKBUILDs and Alpine Linux's aports";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }:
    flake-utils.lib.eachDefaultSystem
      (system:
      let pkgs = nixpkgs.legacyPackages.${system}.extend self.overlays.default;
      in
      {
        # apkbuild-lint is by far the most common and consequential
        # of our provided binaries so make it the default, in the
        # future we might want to provide a minor shellscript
        # that runs apkbuild-lint and secfixes-check
        defaultApp = self.apps.${system}.apkbuild-lint;
        apps = {
          apkbuild-lint = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/apkbuild-lint";
          };
          apkbuild-fixer = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/apkbuild-fixer";
          };
          aports-lint = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/aports-lint";
          };
          initd-lint = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/initd-lint";
          };
          secfixes-check = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/secfixes-check";
          };
          print-go-deps = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/print-go-deps";
          };
          convert-volatile-sources = {
            type = "app";
            program = "${self.packages.${system}.atools}/bin/convert-volatile-sources";
          };
        };
        packages = {
          default = self.packages.${system}.atools;
          inherit (pkgs) atools;
        };
      }) //
    {
      overlays.default = final: prev: {
        atools = final.callPackage ./atools.nix { };
      };
    };
}
